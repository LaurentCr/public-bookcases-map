var mymap = L.map('mapid').setView([46.672, 2.065], 6);

//~ L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    //~ attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    //~ maxZoom: 18,
    //~ id: 'mapbox.streets',
    //~ accessToken: 'pk.eyJ1IjoibGF1cmVudGMiLCJhIjoiY2puMzU2MHoyMGQxODNxcnVwaHQ4cjc5NiJ9.zW8GPUJekZXG_PKJZEytyg'
//~ }).addTo(mymap);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     attribution: '&copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
      maxZoom: 18
}).addTo(mymap);

// create control and add to map
var lc = L.control.locate().addTo(mymap);

// request location update and set location
lc.start();

var opl = new L.OverPassLayer({
	query: 'node[amenity=public_bookcase]({{bbox}});out;',
	minZoom: 8,
	markerIcon: new L.Icon.Default
});

mymap.addLayer(opl);

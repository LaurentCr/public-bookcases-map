# Public Bookcases Map

Map with geolocation using Leaflet and OpenStreetMap data to display all the public bookcases, or microlibraries, in your area (in French, Boîte à lire)

See live demo at https://nc.lcreation.fr:81/boitealire